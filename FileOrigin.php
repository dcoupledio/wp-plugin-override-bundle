<?php namespace Decoupled\PluginOverride;

class FileOrigin{

    public static function isPlugin( $path )
    {
        return !self::isTheme( $path );
    }

    public static function isTheme( $path )
    {
        $tplDir  = get_template_directory();

        $stylDir = get_stylesheet_directory();

        if( strpos( $path, $tplDir ) === 0
            || strpos( $path, $stylDir ) === 0 )
        {
            return true;
        }
    }

}