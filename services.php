<?php 

use Decoupled\PluginOverride\FileOrigin;
use Decoupled\Core\Application\ApplicationContainer;

return function( ApplicationContainer $app ){

    $app['$fileOrigin'] = function(){

        return new FileOrigin();
    };

    /*
    $app['plugin.override.service'] = function(){

        return new PluginOverride\Service();
    };
    */
};