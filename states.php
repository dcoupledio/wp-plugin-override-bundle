<?php return function( Decoupled\Core\State\StateRouter $stateRouter ){

    $stateRouter('plugin.override.default')
        ->when('$app')
        ->uses( 'Decoupled\\PluginOverride\\Controller\\PluginOverrideController@override' );
};