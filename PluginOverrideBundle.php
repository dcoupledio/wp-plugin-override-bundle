<?php namespace Decoupled\PluginOverride;

use Decoupled\Core\Bundle\BundleInterface;

class PluginOverrideBundle implements BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName()
    {
        return 'plugin.override';
    }

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir()
    {
        return dirname(__FILE__);
    }

}
