<?php namespace Decoupled\PluginOverride\Controller;

class PluginOverrideController{

    public function override( $scope, $out, $app, $template )
    {
        if( isset( $scope['overridePlugin'] ) && $scope['overridePlugin'] ) return;

        if( $app['$fileOrigin']->isPlugin( $template ) )
        {
            $view = apply_filters( '$template.legacy', '@app/legacy.html.twig' );

            return $out( $view )->with( $scope );
        }
    }
}